package com.hash.hiring.discount;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;


import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import com.hash.hiring.discount.controller.DiscountGRPCController;
import com.hash.hiring.discount.lib.DiscountRequestProto;
import com.hash.hiring.discount.lib.DiscountResponseProto;
import com.hash.hiring.discount.lib.DiscountServiceProtoGrpc;

import io.grpc.stub.StreamObserver;


@SpringBootTest
@SpringJUnitConfig(classes = {DiscountGRPCController.class})
class DiscountApplicationTests {
	
	  @Autowired
	  private DiscountGRPCController discountGRPC;
	
	  
	  public DiscountServiceProtoGrpc.DiscountServiceProtoBlockingStub stub;
	  
	  

}
