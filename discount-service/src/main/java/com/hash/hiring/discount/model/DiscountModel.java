package com.hash.hiring.discount.model;

public class DiscountModel {
	
	private float percentage;
	
	private Integer valueInCents;

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

	public Integer getValueInCents() {
		return valueInCents;
	}

	public void setValueInCents(Integer valueInCents) {
		this.valueInCents = valueInCents;
	}
	
	@Override
	public String toString() {
		return "DiscountModel [percentage=" + percentage + ", valueInCents=" + valueInCents + "]";
	}

}
