package com.hash.hiring.discount.calculation;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.hash.hiring.discount.model.DiscountModel;

public class DiscountCalculation {
	
	private DiscountCalculationBase calculation;
	
	private static float MAX_DISCOUNT;
	
	public static Integer productPrice;
	
	public static Integer initialProductPrice; 
	
	public static float accumulatedPercentage = 0;
	
	public DiscountCalculation(DiscountCalculationBase calculation) {
		this.calculation =  calculation;
	}

	public DiscountModel calculate(Date date) {
		
		DiscountModel discount  = calculation.calculation(date, productPrice);
		
		return discount !=  null ? calculateControlReturn(discount) : discount;
	} 
	
	public DiscountModel calculate(List<Date> date) {
		
		DiscountModel discount  = calculation.calculation(date, productPrice);
		
		return discount !=  null ? calculateControlReturn(discount) : discount;
	}
	
	public DiscountModel calculateControlReturn(DiscountModel returnedDiscount) {
		
		float tempAccumulatedPercentege = accumulatedPercentage + returnedDiscount.getPercentage();
		
		//control the discount limit
		if(tempAccumulatedPercentege <= MAX_DISCOUNT){
			accumulatedPercentage = tempAccumulatedPercentege;
			productPrice = returnedDiscount.getValueInCents();
		}else {
			accumulatedPercentage = MAX_DISCOUNT;
			productPrice = calculation.calculation(new Date(), initialProductPrice, MAX_DISCOUNT).getValueInCents();
		}
		return returnedDiscount;
	}

	public DiscountCalculationBase getCalculation() {
		return calculation;
	}

	public void setCalculation(DiscountCalculationBase calculation) {
		this.calculation = calculation;
	}
	
	public static void reset() {
		accumulatedPercentage = 0;
		productPrice = null;
	}
	
	public static void setInitialCalculationConfig(Integer productPriceinCents, float maxDiscountAllowed) {
		initialProductPrice = productPriceinCents;
		productPrice = productPriceinCents;
		MAX_DISCOUNT = maxDiscountAllowed;
	}
	
	public Date getNextBirthDay(Date birthDate) {
		
		Calendar calendarBirthdate = Calendar.getInstance();
		calendarBirthdate.setTime(birthDate);
		
		Calendar currentCalendar = Calendar.getInstance();
		
		currentCalendar.set(currentCalendar.get(Calendar.YEAR), calendarBirthdate.get(Calendar.MONTH), calendarBirthdate.get(Calendar.DAY_OF_MONTH));
		
		return currentCalendar.getTime();
	}

}
