package com.hash.hiring.discount.calculation;

import java.util.Date;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hash.hiring.discount.model.DiscountModel;

public class BirthdateCalculation extends DiscountCalculationBase {
	
	private static final Logger logger = LogManager.getLogger(BirthdateCalculation.class);
	
	@Override
	public DiscountModel calculation(Date receivedDate, Integer priceInCents) {
		
		return  super.calculation(receivedDate, priceInCents, (float) 5.0);
	}

}
