package com.hash.hiring.discount.lib;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * Service about discount product calculations.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.35.0)",
    comments = "Source: .proto")
public final class DiscountServiceProtoGrpc {

  private DiscountServiceProtoGrpc() {}

  public static final String SERVICE_NAME = "com.hash.hiring.discount.DiscountServiceProto";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.hash.hiring.discount.lib.DiscountRequestProto,
      com.hash.hiring.discount.lib.DiscountResponseProto> getGetDiscountMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getDiscount",
      requestType = com.hash.hiring.discount.lib.DiscountRequestProto.class,
      responseType = com.hash.hiring.discount.lib.DiscountResponseProto.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.hash.hiring.discount.lib.DiscountRequestProto,
      com.hash.hiring.discount.lib.DiscountResponseProto> getGetDiscountMethod() {
    io.grpc.MethodDescriptor<com.hash.hiring.discount.lib.DiscountRequestProto, com.hash.hiring.discount.lib.DiscountResponseProto> getGetDiscountMethod;
    if ((getGetDiscountMethod = DiscountServiceProtoGrpc.getGetDiscountMethod) == null) {
      synchronized (DiscountServiceProtoGrpc.class) {
        if ((getGetDiscountMethod = DiscountServiceProtoGrpc.getGetDiscountMethod) == null) {
          DiscountServiceProtoGrpc.getGetDiscountMethod = getGetDiscountMethod =
              io.grpc.MethodDescriptor.<com.hash.hiring.discount.lib.DiscountRequestProto, com.hash.hiring.discount.lib.DiscountResponseProto>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getDiscount"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.hash.hiring.discount.lib.DiscountRequestProto.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.hash.hiring.discount.lib.DiscountResponseProto.getDefaultInstance()))
              .setSchemaDescriptor(new DiscountServiceProtoMethodDescriptorSupplier("getDiscount"))
              .build();
        }
      }
    }
    return getGetDiscountMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DiscountServiceProtoStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<DiscountServiceProtoStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<DiscountServiceProtoStub>() {
        @java.lang.Override
        public DiscountServiceProtoStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new DiscountServiceProtoStub(channel, callOptions);
        }
      };
    return DiscountServiceProtoStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DiscountServiceProtoBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<DiscountServiceProtoBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<DiscountServiceProtoBlockingStub>() {
        @java.lang.Override
        public DiscountServiceProtoBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new DiscountServiceProtoBlockingStub(channel, callOptions);
        }
      };
    return DiscountServiceProtoBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DiscountServiceProtoFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<DiscountServiceProtoFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<DiscountServiceProtoFutureStub>() {
        @java.lang.Override
        public DiscountServiceProtoFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new DiscountServiceProtoFutureStub(channel, callOptions);
        }
      };
    return DiscountServiceProtoFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * Service about discount product calculations.
   * </pre>
   */
  public static abstract class DiscountServiceProtoImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Method to get a product discount
     * </pre>
     */
    public void getDiscount(com.hash.hiring.discount.lib.DiscountRequestProto request,
        io.grpc.stub.StreamObserver<com.hash.hiring.discount.lib.DiscountResponseProto> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetDiscountMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetDiscountMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.hash.hiring.discount.lib.DiscountRequestProto,
                com.hash.hiring.discount.lib.DiscountResponseProto>(
                  this, METHODID_GET_DISCOUNT)))
          .build();
    }
  }

  /**
   * <pre>
   * Service about discount product calculations.
   * </pre>
   */
  public static final class DiscountServiceProtoStub extends io.grpc.stub.AbstractAsyncStub<DiscountServiceProtoStub> {
    private DiscountServiceProtoStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountServiceProtoStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new DiscountServiceProtoStub(channel, callOptions);
    }

    /**
     * <pre>
     * Method to get a product discount
     * </pre>
     */
    public void getDiscount(com.hash.hiring.discount.lib.DiscountRequestProto request,
        io.grpc.stub.StreamObserver<com.hash.hiring.discount.lib.DiscountResponseProto> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetDiscountMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Service about discount product calculations.
   * </pre>
   */
  public static final class DiscountServiceProtoBlockingStub extends io.grpc.stub.AbstractBlockingStub<DiscountServiceProtoBlockingStub> {
    private DiscountServiceProtoBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountServiceProtoBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new DiscountServiceProtoBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Method to get a product discount
     * </pre>
     */
    public com.hash.hiring.discount.lib.DiscountResponseProto getDiscount(com.hash.hiring.discount.lib.DiscountRequestProto request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetDiscountMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Service about discount product calculations.
   * </pre>
   */
  public static final class DiscountServiceProtoFutureStub extends io.grpc.stub.AbstractFutureStub<DiscountServiceProtoFutureStub> {
    private DiscountServiceProtoFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DiscountServiceProtoFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new DiscountServiceProtoFutureStub(channel, callOptions);
    }

    /**
     * <pre>
     * Method to get a product discount
     * </pre>
     */
    public com.google.common.util.concurrent.ListenableFuture<com.hash.hiring.discount.lib.DiscountResponseProto> getDiscount(
        com.hash.hiring.discount.lib.DiscountRequestProto request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetDiscountMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_DISCOUNT = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DiscountServiceProtoImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DiscountServiceProtoImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_DISCOUNT:
          serviceImpl.getDiscount((com.hash.hiring.discount.lib.DiscountRequestProto) request,
              (io.grpc.stub.StreamObserver<com.hash.hiring.discount.lib.DiscountResponseProto>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DiscountServiceProtoBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DiscountServiceProtoBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.hash.hiring.discount.lib.DiscountProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("DiscountServiceProto");
    }
  }

  private static final class DiscountServiceProtoFileDescriptorSupplier
      extends DiscountServiceProtoBaseDescriptorSupplier {
    DiscountServiceProtoFileDescriptorSupplier() {}
  }

  private static final class DiscountServiceProtoMethodDescriptorSupplier
      extends DiscountServiceProtoBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DiscountServiceProtoMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DiscountServiceProtoGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DiscountServiceProtoFileDescriptorSupplier())
              .addMethod(getGetDiscountMethod())
              .build();
        }
      }
    }
    return result;
  }
}
