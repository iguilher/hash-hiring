package com.hash.hiring.discount.repository;

import com.hash.hiring.discount.model.ParameterModel;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IParameterRepository extends CrudRepository<ParameterModel, String> {
	
	 @Query("SELECT param.dateValue FROM ParameterModel as param WHERE param.groupName = :groupName")
	 List<Date> getParameterByGroupName(@Param("groupName") String groupName);
}
