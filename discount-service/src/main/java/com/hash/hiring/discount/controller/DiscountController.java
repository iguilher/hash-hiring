package com.hash.hiring.discount.controller;

import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hash.hiring.discount.model.DiscountModel;
import com.hash.hiring.discount.model.ParameterModel;
import com.hash.hiring.discount.service.DiscountService;

import javassist.NotFoundException;

/**
 * Controller responsible to receive RESTfull request and call service to calculate product discount
 * @author iguilherme3
 * 
 */
@RestController
@RequestMapping("/api")
public class DiscountController {
	
	@Autowired
	private DiscountService discountService;
	
	private static final Logger logger = LogManager.getLogger(DiscountController.class);
	
	@GetMapping("/discount/{productId}")
	public ResponseEntity<String> discount(@PathVariable String productId, @RequestHeader("X-USER-ID") String userId) {
		DiscountModel discount = new DiscountModel();
		try {
			discount = discountService.discountCalculation(userId, productId);
		} catch (NoSuchElementException  e) {
			logger.error("onError : {}" , e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_ACCEPTABLE);
		}catch (NotFoundException e) {
			logger.error("onError : {}" , e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
		}catch (Exception e) {
			logger.error("onError : {}" , e.getMessage());
			return new ResponseEntity<String>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		logger.info("Discount return: {} ", discount);
		  
		return new ResponseEntity<String>(discount.toString(), HttpStatus.OK);

	}

}
