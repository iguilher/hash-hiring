package com.hash.hiring.discount.util;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hash.hiring.discount.repository.IParameterRepository;

/**
 * This Bean is responsable to invoke the static {@link Util} on startup application
 * @author iguilherme3
 *
 */
@Component
public class UtilBean {
	
	@Autowired
	private IParameterRepository parameterRepository;
	
	@PostConstruct
	public void init(){
		Util.getDiscountDays(parameterRepository);
	}

}
