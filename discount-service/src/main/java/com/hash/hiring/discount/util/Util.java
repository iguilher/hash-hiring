package com.hash.hiring.discount.util;


import java.util.Date;
import java.util.List;

import com.hash.hiring.discount.repository.IParameterRepository;

/**
 * This Final Class is instantiated on startup application 
 * @author iguilherme3
 *
 */

public final class Util {
	
	public static List<Date> parameterListModel;
	
	/**
	 * getDiscountDays receive the discount days parameterized on startup aplication(static)
	 * @param parameterRepository
	 */
	public static void getDiscountDays(IParameterRepository parameterRepository) {
		parameterListModel = parameterRepository.getParameterByGroupName("DISCOUNT_DAYS");
	}
}
