package com.hash.hiring.discount.repository;

import com.hash.hiring.discount.model.ProductModel;

import org.springframework.data.repository.CrudRepository;

public interface IProductRepository extends CrudRepository<ProductModel, String> {

}
