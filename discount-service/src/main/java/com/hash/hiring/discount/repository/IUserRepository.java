package com.hash.hiring.discount.repository;

import com.hash.hiring.discount.model.UserModel;

import org.springframework.data.repository.CrudRepository;

public interface IUserRepository extends CrudRepository<UserModel, String> {

}
