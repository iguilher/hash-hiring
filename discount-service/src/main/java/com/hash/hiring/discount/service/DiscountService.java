package com.hash.hiring.discount.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hash.hiring.discount.calculation.BirthdateCalculation;
import com.hash.hiring.discount.calculation.DiscountCalculation;
import com.hash.hiring.discount.calculation.DiscountDaysCalculation;
import com.hash.hiring.discount.model.DiscountModel;
import com.hash.hiring.discount.model.ProductModel;
import com.hash.hiring.discount.model.UserModel;
import com.hash.hiring.discount.repository.IProductRepository;
import com.hash.hiring.discount.repository.IUserRepository;
import com.hash.hiring.discount.util.Util;

import javassist.NotFoundException;

/**
 * This Service is responsable to manager and construct the product discount
 * @author iguilherme3
 *
 */
@Service
public class DiscountService {
	
	private static final Logger logger = LogManager.getLogger(DiscountService.class);
	
	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	private IProductRepository productRepository;
	
	public DiscountModel discountCalculation(String userId, String productId) throws NoSuchElementException, NotFoundException {
		
		DiscountModel result =  new DiscountModel();

		try {
			if(StringUtils.isBlank(productId)) {
				throw new NotFoundException("Product ID not informed");
			}
			
			ProductModel product  = productRepository.findById(productId).orElseThrow();
			//assign the initial product price and max discount allowed
			DiscountCalculation.setInitialCalculationConfig(product.getPriceInCents(), (float) 10.6);
			
			DiscountCalculation blackFriday =  new DiscountCalculation(new DiscountDaysCalculation());
			blackFriday.calculate(Util.parameterListModel);
			
			if(!StringUtils.isBlank(userId)) {
				UserModel user  = userRepository.findById(userId).orElseThrow();
				DiscountCalculation birthdate =  new DiscountCalculation(new BirthdateCalculation());
				birthdate.calculate(birthdate.getNextBirthDay(user.getDateOfBirth()));
			}
			
			result.setPercentage((DiscountCalculation.accumulatedPercentage));
			result.setValueInCents(DiscountCalculation.productPrice);
			
		} finally {
			DiscountCalculation.reset();
		}
		
		return result;
		
	}
}
