package com.hash.hiring.discount.controller;

import java.util.NoSuchElementException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import com.hash.hiring.discount.lib.DiscountRequestProto;
import com.hash.hiring.discount.lib.DiscountResponseProto;
import com.hash.hiring.discount.lib.DiscountServiceProtoGrpc;
import com.hash.hiring.discount.model.DiscountModel;
import com.hash.hiring.discount.service.DiscountService;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import javassist.NotFoundException;
import net.devh.boot.grpc.server.service.GrpcService;

/** 
 * Responsable controller to receive RPC client invocations and call the service to get product discount
 * @author iguilherme3
 *
 */
@Controller
@GrpcService
public class DiscountGRPCController extends DiscountServiceProtoGrpc.DiscountServiceProtoImplBase {
	
	@Autowired
	private DiscountService discountService;
	
	private static final Logger logger = LogManager.getLogger(DiscountGRPCController.class);
	
	@Override
	public void getDiscount(DiscountRequestProto request, StreamObserver<DiscountResponseProto> responseObserver) {
		logger.info("Request gRPC received: {} ", request);
		
		try {
			DiscountModel discount = discountService.discountCalculation(request.getUserId(), request.getProductId());
			DiscountResponseProto response = DiscountResponseProto.newBuilder()
					.setPercentage(discount.getPercentage())
					.setValueInCents(discount.getValueInCents())
					.build();

	        responseObserver.onNext(response);
	        responseObserver.onCompleted();
		} catch (NoSuchElementException  e) {
			logger.error("onError : {}" , e.getMessage());
			responseObserver.onError(new StatusRuntimeException(Status.INVALID_ARGUMENT.withDescription(e.getMessage())));
		}catch (NotFoundException e) {
			logger.error("onError : {}" , e.getMessage());
			responseObserver.onError(new StatusRuntimeException(Status.NOT_FOUND.withDescription(e.getMessage())));
		}catch (Exception e) {
			logger.error("onError : {}" , e.getMessage());
			responseObserver.onError(new StatusRuntimeException(Status.INTERNAL.withDescription(e.getMessage())));
		}
	}
}
