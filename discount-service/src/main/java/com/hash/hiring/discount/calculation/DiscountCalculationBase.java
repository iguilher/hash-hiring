package com.hash.hiring.discount.calculation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hash.hiring.discount.model.DiscountModel;

public class DiscountCalculationBase  {
	
	private static final Logger logger = LogManager.getLogger(DiscountCalculationBase.class);
	
	private static final float DEFAULT_DISCOUNT = (float) 0.10;

	public DiscountModel calculation(Date receivedDate, Integer priceInCents, float percentageDiscount) {
		
		DiscountModel discountReturn = new DiscountModel();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		format.setTimeZone(TimeZone.getTimeZone("Brazil/East"));
		
		if (format.format(receivedDate).equals(format.format(new Date()))) {
			//dividing by 100 for the result to be positive
			float percentageDiscountFinal = (float) (percentageDiscount / 100.0);
		    Integer result = (int) (priceInCents - (priceInCents *  percentageDiscountFinal ));
		    discountReturn.setValueInCents(result);
		    discountReturn.setPercentage(percentageDiscount);
		    return discountReturn;
		}

		return null;
	}
	
	public DiscountModel calculation(Date date, Integer priceInCents) {
		return calculation(date, priceInCents, DEFAULT_DISCOUNT);
	}

	public DiscountModel calculation(List<Date> listDates, Integer priceInCents) {
		DiscountModel discountReturn = new DiscountModel(); 
		for(Date date :  listDates) {
			discountReturn = calculation(date, priceInCents);
			
			if(Objects.nonNull(discountReturn)) {
				break;
			}
		}
		return discountReturn;
	}
	
	public DiscountModel calculation(List<Date> listDates, Integer priceInCents, float percentageDiscount) {
		DiscountModel discountReturn = new DiscountModel(); 
		for(Date date :  listDates) {
			discountReturn = calculation(date, priceInCents, percentageDiscount);
			
			if(Objects.nonNull(discountReturn)) {
				break;
			}
		}
		return discountReturn;
	}

}
