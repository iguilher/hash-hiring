package com.hash.hiring.discount.calculation;

import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.hash.hiring.discount.model.DiscountModel;

public class DiscountDaysCalculation extends DiscountCalculationBase {
	
	private static final Logger logger = LogManager.getLogger(DiscountDaysCalculation.class);

	@Override
	public DiscountModel calculation(Date receivedDate, Integer priceInCents) {
		return  super.calculation(receivedDate, priceInCents, (float) 10.0);
	}
	
	@Override
	public DiscountModel calculation(List<Date> listDates, Integer priceInCents) {
		return  super.calculation(listDates, priceInCents, (float) 10.0);
	}

}
