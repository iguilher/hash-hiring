INSERT INTO hh_user(id, created_at, updated_at, date_of_birth, first_name, last_name) 
VALUES ('d4f8924a-1d13-479d-aae2-0ab1f2a2b7a3', now(), now(), '1996-09-16', 'Admin', 'Admin');

INSERT INTO hh_product(
	id, created_at, updated_at, description, price_in_cents, title)
	VALUES ('ea28d7bb-78ef-4375-9888-e800a410b73a', now(), now(), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 50090, 'Notebook XPTO');
	
INSERT INTO hh_product(
	id, created_at, updated_at, description, price_in_cents, title)
	VALUES ('b738e721-ae2b-4b17-9d26-a8dfeb609b01', now(), now(), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 12050, 'Tablet XPTO');
	
INSERT INTO hh_product(
	id, created_at, updated_at, description, price_in_cents, title)
	VALUES ('6b11003c-b8b5-499c-a826-66f9d6531255', now(), now(), 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.', 8090, 'Headphone XPTO');

INSERT INTO hh_parameter(
	id, created_at, updated_at, date_value, group_name, name)
	VALUES ('15256276-83bd-45f8-98b6-06c7944da616', now(), now(), now(), 'DISCOUNT_DAYS', 'Days to apply 10 percent of discount in all products');