# Hash-hiring

Hash Hiring project

### Objective
This repository has the codes about the challege to Hash hire. Link aboute the challege is [here] (https://github.com/hashlab/hiring/blob/master/challenges/pt-br/back-challenge.md)


### Technologies
This project was delveloped using:

* Discount Service:
	* Java 11
	* [Spring boot Framework] (https://spring.io/projects/spring-boot)
	* JPA - Hibernate - ORM
	* [Postgress] (https://www.postgresql.org/)
	* gRPC Server
	* Docker
* Product Service:
	* Go lang 1.16
	* [GIN Gonic WEB Framework] (https://github.com/gin-gonic/gin)
	* [go-pg Postegress - ORM] (https://github.com/go-pg/pg/)
	* [Postgres] (https://www.postgresql.org/)

### Architecture

REST endpoint /product return the list product items. The product service request /discount service using RPC(Remote Procedure Call) to access the method in the /discount service. The both services access Postgres database.

![applications architecture image](images/architecture.png)

### How to execute

Clone this repository. 

Open the foder ./discount-service on CLI and RUN:

```
docker build -t gitlab.com/iguilher/discount-service .
```
After finished, open the foder ./product-service on CLI and RUN:

```
docker build -t gitlab.com/iguilher/product-service .
```
After finished, on the root repository path, RUN:  

```
docker-compose up.
```

Now is possible to request the endpoint http://localhost:8000/product/ on browser, http client or if you want.

NOTE: The discount-service load initial data to test(products, user and parammter) on Postegres database. This was do to help use on  hashers evaluation. If you need to stop the containers and start up again, is necessary to RUN above commands to exclude da loaded data and allow the spring-boot insert again in /product-service startup:

```
docker ps -a 
```
Get the Container ID of the  image postgres:10.4, and RUN:
```
docker rm <container_postgres_id>
```
Now, you can run the docker compose again.