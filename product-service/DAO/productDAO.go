package DAO

import (	
	"gitlab.com/iguilher/product-service/model"
	"gitlab.com/iguilher/product-service/util"
)

func SelectProduct(prod model.Product) (model.Product, error) {
	conn := util.NewDBConn()
	err := conn.Model(&prod).WherePK().Select()
	return prod, err
 }

 func SelectAllProducts() ([]model.Product, error) {
	conn := util.NewDBConn()
	var products []model.Product 
	err := conn.Model(&products).Limit(50).Select()
	return products, err
 }