package util

import (
	"log"

	"github.com/go-pg/pg/v10"

	//"github.com/go-pg/pg/v10/orm"
	"encoding/json"
	"os"

	"gitlab.com/iguilher/product-service/model"
	"google.golang.org/grpc"
)

var EnviConfig model.Enviromment

func NewDBConn() (con *pg.DB) {
	options := &pg.Options{
		User:     EnviConfig.DBUser,
		Password: EnviConfig.DBPass,
		Addr:     EnviConfig.DBAddress,
		Database: EnviConfig.DBSchema,
		PoolSize: 50,
	}
	con = pg.Connect(options)
	if con == nil {
		log.Fatal("cannot connect to postgres")
	}
	return
}

func NewgRPCConn() *grpc.ClientConn {
	GRPCConn, err := grpc.Dial(EnviConfig.DiscountServiceEndpoint, grpc.WithInsecure())

	if err != nil {
		log.Printf("Error when Dial gRPC server connect: %s", err)
	}

	return GRPCConn
}

func ReadConfigFile() {
	envOption := "./env.json"
	if os.Getenv("ENV") == "PROD" {
		envOption = "./env-docker.json"
	}

	file, err := os.Open(envOption)
	if err != nil {
		panic(err)
	}
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&EnviConfig)
	if err != nil {
		panic(err)
	}
}
