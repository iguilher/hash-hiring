package controller

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"gitlab.com/iguilher/product-service/service"
	"gitlab.com/iguilher/product-service/model"
)

func GetProduct(context *gin.Context) {

	product := model.Product{Id: context.Param("id")}
	prod := service.GetProduct(product);

	log.Printf( "Welcome to the controller! " +  prod.Title)

	context.JSON(http.StatusOK, prod)
}

func GetProducts(context *gin.Context) {

	headerModel := model.Header{}

	err := context.ShouldBindHeader(&headerModel);

	if err != nil{
		context.JSON(500, err)
	}
	
	products := service.GetProducts(headerModel)
	context.JSON(http.StatusOK, products)
}