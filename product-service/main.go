package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/iguilher/product-service/controller"
	"gitlab.com/iguilher/product-service/util"
)

func main() {

	log.Printf("Varivel banco: " + os.Getenv("DB_HOST"))

	server := InitialConfigurations()

	server.Run(":" + util.EnviConfig.Port)
}

func InitialConfigurations() *gin.Engine {

	util.ReadConfigFile()

	webServer := gin.Default()

	webServer.GET("/product/", controller.GetProducts)
	webServer.GET("/product/:id", controller.GetProduct)

	return webServer
}
