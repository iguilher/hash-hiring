package main

import (
	"testing"
	"net/http/httptest"
	"net/http"
	//"encoding/json"
	"github.com/gin-gonic/gin"
)

func TestGetProducts(t *testing.T) {

	r := InitialConfigurations()
	
	// Create a request to send to the above route
	req, _ := http.NewRequest("GET", "/product/", nil)

	testHTTPResponse(t, r, req, func(w *httptest.ResponseRecorder) bool {

		return w.Code == http.StatusOK
	})

}

func testHTTPResponse(t *testing.T, r *gin.Engine, req *http.Request, f func(w *httptest.ResponseRecorder) bool) {

	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	if !f(w) {
		t.Errorf("Error, the request returned with unsuccess HTTP status")
		t.Fail()
	}
}

