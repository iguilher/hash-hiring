package service

import (
	"log"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"gitlab.com/iguilher/product-service/model"
	"gitlab.com/iguilher/product-service/DAO"
	"gitlab.com/iguilher/product-service/proto"
	"gitlab.com/iguilher/product-service/util"
)
var connProductService *grpc.ClientConn

func GetProducts(header model.Header) ([]model.Product)  {
	var productsReturn []model.Product

	productsReturn, err  := DAO.SelectAllProducts()
	if err != nil {
        panic(err)
	}
	connProductService = util.NewgRPCConn()
	for index, productItem := range productsReturn {
		productsReturn[index].Discount = gRPCProductClient(productItem, header.UserId)
	}
	defer connProductService.Close()

	return productsReturn
}

func gRPCProductClient(product model.Product, userId string)(model.Discount){

	var dicountReturn model.Discount
	
	client := proto.NewDiscountServiceProtoClient(connProductService)
	response, err := client.GetDiscount(context.Background(), &proto.DiscountRequestProto{ProductId: product.Id, UserId:userId })

	if err != nil {
		//If occurred an error, just log the information
		log.Printf("Error when calling GetDiscount: %s", err)
		return dicountReturn
	}
	dicountReturn = model.Discount{Percentage: response.Percentage, ValueInCents: response.ValueInCents}

	return dicountReturn

}

func GetProduct(product model.Product) (model.Product) {

	prod, err  := DAO.SelectProduct(product)
	if err != nil {
        panic(err)
	}
	return prod
}