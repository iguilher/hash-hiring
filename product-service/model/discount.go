package model

type Discount struct {
	Percentage        float32   `json:"percentage"`
	ValueInCents      int32   `json:"valueInCents"`
}