package model

type Product struct {
	tableName struct{} `pg:"hh_product"`
	Id        string   `json:"id" pg:"id,pk"`
	PriceInCents      int   `json:"priceInCents,omitempty" pg:"price_in_cents"`
	Title        string   `json:"title,omitempty" pg:"title"`
	Description    string   `json:"description,omitempty" pg:"description"`
	Discount Discount `json:"discount,omitempty" pg:"-"`
}