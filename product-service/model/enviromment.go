package model

type Enviromment struct {
	Port   string `json:"webPort"`
	DBUser string `json:"dbuser"`
	DBPass string `json:"dbpass"`
	DBAddress string `json:"dbaddress"`
	DBSchema string `json:"dbschema"`
	DiscountServiceEndpoint string `json:"discountService"`
}